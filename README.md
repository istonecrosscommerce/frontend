# iStone Frontend Library

```
 ______   ______          __     ______    
/\  ___\ /\  ___\        /\ \   /\  ___\   
\ \  __\ \ \  __\       _\_\ \  \ \___  \  
 \ \_\    \ \_____\    /\_____\  \/\_____\
  \/_/     \/_____/    \/_____/   \/_____/
  
  
```

### Rules
1. **Write functions** and put them in separate folders under the appropiate main folder (jQuery, Vanilla, etc).
2. **Name your function folders like this:**
    * `Name of function (Your name) Customer name`
3. **Write a README۔md file**
    * This will be shown in Bitbucket automatically and give a good overview for others (test your Markdown with [Dillinger](http://dillinger.io) and check the [supported syntax here](https://bitbucket.org/tutorials/markdowndemo))
4. **Always use `js-class` to hook your functions**

5. **Start your function file like this.**
```

$(function() {
	// call the function
    funcName(); 
});
    
function funcName() {
	// hook the function with the js-class
    var variableName = $('.js-class');
    
    //See if the class exist otherwise don't run the function.
    if (!variableName.length) {
        return;
    }

    //put the code for the function here.
}

```
