## Synopsis
A super-simple <1KB type checking module for JavaScript that returns a Boolean for each type check.

### Usage

```javascript
axis.isArray([]);                // true
axis.isObject({});               // true
axis.isString('');               // true
axis.isDate(new Date());         // true
axis.isRegExp(/test/i);          // true
axis.isFunction(function(){});   // true
axis.isBoolean(true);            // true
axis.isNumber(1);                // true
axis.isNull(null);               // true
axis.isUndefined();              // true
```

### Installing with npm

`npm install axis.js`

### Installing with Bower
Use the repository hook:

`bower install https://github.com/toddmotto/axis.git`
