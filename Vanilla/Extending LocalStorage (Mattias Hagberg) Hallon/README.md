## Synopsis

Extending the native `localStorage` property to make life a little bit easier, and allowing us to store `objects`.

## Code Example

When using the native `localStorage.setItem` method, you can set and get key-value pairs in the following format:

```javascript
localStorage.setItem(keyName, keyValue);
```
* `keyName`
    * A DOMString containing the name of the key you want to create/update.
* `keyValue`
    * A DOMString containing the value you want to give the key you are creating/updating.

```javascript
localStorage.setItem('username', 'John');

return localStorage.getItem('username');
```

However, what this extension does is that it allows us to store entire objects as the given value, instead of only a string. This gives us:

```javascript
setObj('keyName', keyObject);
```
* `keyName`
    * A DOMString containing the name of the key you want to create/update.
* `keyObject`
    * An object containing all the values you want to give the key you are creating/updating.

```
setObj('myObj', myObject);

return getObj('myObj');
```

### Import and usage example

```javascript
import { setObj, getObj } from './filename'

let eObject = { myProperty: 'value' }

let set = setObj('myObj', eObject);
let get = getObj('myObj');

console.log(get.myProperty); // will return myObj.myProperty from localStorage
```

## Installation

The scripts are currently exported as ES6 modules - however, you can easily wrap them as regular functions instead. The end result will be the same.
