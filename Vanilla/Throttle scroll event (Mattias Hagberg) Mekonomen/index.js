var scrollTimeout;

function scrollThrottle() {
    if (!scrollTimeout) {
        scrollTimeout = setTimeout(function() {
            scrollTimeout = null;
            // Add what you want below - it will execute at a rate of 15fps

            console.log('Scrolling');
        }, 66);
    }
}

window.addEventListener('scroll', scrollThrottle, false);
