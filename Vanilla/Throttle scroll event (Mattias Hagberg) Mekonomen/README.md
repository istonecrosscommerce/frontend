## Synopsis
A super-simple <0.5KB scroll throttle function, so as to avoid performance issues that may happen when adding listeners to scroll events. Per default, `addEventListener('scroll')` will execute for every pixel scrolled - by adding this throttle as-is, it will execute at a rate of 15fps instead.

### Usage
Just copy-paste the code in `index.js` and use accordingly.
