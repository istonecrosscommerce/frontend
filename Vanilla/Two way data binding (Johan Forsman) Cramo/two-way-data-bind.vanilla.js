// Requires DOM

const watcher = document.querySelector('.data-watcher');

watcher.addEventListener('keyup', changeHandler);

function changeHandler(event) {
    const target = event.target.dataset.target;
    const val = event.target.value;
    const el = document.querySelector('.data-watch-' + target).innerHTML = val;
}
