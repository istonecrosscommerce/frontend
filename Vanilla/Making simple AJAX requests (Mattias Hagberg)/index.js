/**
 * window.Ajax - Minimal function to simplify AJAX requests
 *
 * @param  {string} url         URL to resource
 * @param  {string} method      Defaults to 'GET'
 * @param  {object} data        Data to send with e.g. 'POST'
 * @param  {string} contentType Optional Content-Type setting
 * @return {promise}            Resolving and rejecting is handled on call
 */

(function (window) {
    const Ajax = (url, method, data, contentType) => {
        return new Promise((resolve, reject) => {
            const req = new XMLHttpRequest();
            method = method ? method : 'GET';
            req.open(method, url);
            contentType = contentType ? req.setRequestHeader('Content-Type', contentType) : null;
            req.onload = () => req.readyState === 4 ? resolve(req.response) : reject(Error(req.statusText));
            req.onerror = e => reject(Error(`Network Error: ${e}`));
            data = JSON.stringify(data);
            data ? req.send(data) : req.send();
        });
    };
    window.Ajax = Ajax;
})(window, document);


/**
 * Sample GET request
 *
 * Will return Array with objects
 *
 * We can do this as a one-liner when we only want to return the data or error
 */

const url = 'https://jsonplaceholder.typicode.com/posts';

window.Ajax(url).then(data => JSON.parse(data)).catch(error => error);


/**
 * Sample POST request
 *
 * Will return the object 'data'
 */

let data = {
    title: 'foo',
    body: 'bar',
    userId: 1
};

window.Ajax(url, 'POST', data, 'application/json')
    .then(data => JSON.parse(data))
    .catch(error => error);
