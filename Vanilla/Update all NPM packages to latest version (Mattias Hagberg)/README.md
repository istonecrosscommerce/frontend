## Synopsis
If you want to update *all* your NPM packages with a single command, here's how to do it. It will update every package to their latest **major** versions - if you simply wish to update all packages to the latest minor version, you can run `npm update -S/-D`, where `-S/-D` means the updated versions will also be adjusted in your package.json dependencies (`-S`) and devDependencies (`-D`), respectively.

### Usage

Save the wipe-dependencies.js file in the project root, and in your package.json, add the following script:

```json
"scripts": {
    "update:packages": "node wipe-dependencies.js && rm -rf node_modules && npm update --save && npm update --save-dev"
}
```

Then simply run `npm run update:packages`, and you're all set!

### wipe-dependencies.js explained

Using `fs`, we read our package.json file, modify the content, and then write them back to the file. The basic idea is to just wipe all the version numbers and replacing them with asterisks. Some basic regular expressions make sure we only update packages which are part of the NPM registry, so as to not break any other external packages.
