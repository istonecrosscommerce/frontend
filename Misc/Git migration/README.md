## Migrating a git repository
> Migrate a repo with all its branches and tags to a new remote, while keeping the entire commit history

Sometimes you need to move your existing git repository to a new remote origin. Here are a few simple and quick steps that does exactly this.

### 1. Make sure you have a local copy of all "old repo" branches and tags

```bash
# Fetch all remote branches and tags
git fetch origin

# View all "old repo" local and remote branches
git branch -a
```

You need to have a *local copy* of **all** remote branches. If you don't have these, save and run `getAllBranches.sh` from your project root:

```bash
# If some of the remotes/branches don't have a local copy,
# run getAllBranches.sh to create a local copy of the missing ones

chmod +x getAllBranches.sh
sh getAllBranches.sh

# Now we have all the remote branches saved locally!
```

### 2. Add the "new repo" as a new remote origin:

```bash
git remote add new-origin git@github.com:user/repo.git
```

### 3. Push all local branches and tags to the "new repo"

```bash
# Push all local branches to new-origin
git push --all new-origin

# Push all tags (if you have any)
git push --tags new-origin
```

### 4. Remove "old repo" origin and its dependencies

```bash
# View existing remotes (you'll see at least 2 remotes for both fetch and push)
git remote -v

# Remove "old repo" remote
git remote rm origin

# Rename new-origin remote to just 'origin'
git remote rename new-origin origin
```

#### Done! Now your local git repo is connected to "new repo" remote which has all the branches, tags and commit history
