### How to do it
Complete the following steps (with sudo permissions):

1. Add `sound-off.sh` and `sound-on.sh` to `/Library/Scripts/`
2. Run the following commands:
```bash
sudo chmod u+x /Library/Scripts/sound-off.sh /Library/Scripts/sound-on.sh
sudo defaults write com.apple.loginwindow LogoutHook /Library/Scripts/sound-off.sh
sudo defaults write com.apple.loginwindow LoginHook /Library/Scripts/sound-on.sh
```

### How it works
The shell scripts are added as hooks for the logout and login procedures. Both these hooks execute an `osascript` command, muting all output sound during login/shutdown, and subsequently unmuting during login/startup.

The `osascript` command is used for executing AppleScripts and other Open Scripting Architecture (OSA) language scripts, and executes the given script files (or stdin if no file is provided).

#### Why use hooks?
Most search results for this issue all seem to provide the same solution, which is:
```bash
sudo nvram SystemAudioVolume=%80   # turn off chime
sudo nvram -d SystemAudioVolume    # turn on chime
```
This, however, doesn't seem to provide a permanent solution, and doesn't even seem to work at all with the last couple of updates to macOS. Using the login/logout hooks provided here seem to do the trick though, and is permanent.

#### Reverse procedure
To reverse this procedure, do the following:

1. Delete the `sound-off.sh` file and run:
    * `sudo defaults delete com.apple.loginwindow LogoutHook`
2. Reboot and log back in
3. Delete the `sound-on.sh` file and run:
    * `sudo defaults delete com.apple.loginwindow LoginHook`
4. You're done!

#### Source
[JacksAdvice - How To Turn Off The Mac Startup Sound (Alex Krenshaw, November 6, 2018)](https://jacksadvice.com/turn-off-the-mac-startup-sound/1515/)
