# Using SVG spritesheets and SVGO
Using SVGO in conjunction with SVG spritesheets creates a compressed and easy-to-use SVG file containing all your projects' icons. It takes just a few seconds to get started.

```bash
npm install -g svgo

 # optimize a single icon:
svgo my_icon.svg

 # or if you have multiple icons:
svgo *.svg

 # convert all icons in a folder and output into a different folder:
svgo -f ../path/to/folder/with/svg/originals -o ../path/to/folder/with/svg/output
```

See [SVGO on Github](https://github.com/svg/svgo#usage) for more usage examples, it's very flexible.

**Other ways to use SVGO:**

* [SVGOMG web interface](https://jakearchibald.github.io/svgomg/)
* [Grunt task](https://github.com/sindresorhus/grunt-svgmin)
* [Gulp task](https://github.com/ben-eb/gulp-svgmin)
* [Webpack loader](https://github.com/tcoopman/image-webpack-loader)
* [PostCSS plugin](https://cssnano.co/optimisations/svgo)
* [Rollup plugin](https://github.com/porsager/rollup-plugin-svgo)
* [NodeJS module](https://github.com/svg/svgo/tree/master/examples)

### Why use SVG at all?
SVG sprites help increase speed, maintain a consistent development workflow, and make the creation of icons much faster. Font icons are well known to have a single colour limitation, while SVGs support multiple colours as well as gradients and other graphical features. Also, browsers consider font icons as text, so the icons are anti-aliased as such. This can lead to icons not being as sharp as you might expect, which won't happen when you use SVG.

A big caveat of using icon fonts is that they can be quite frustrating to position. The icons are inserted via pseudo elements, and it depends on `line-height`, `vertical-align`, `letter-spacing`, `word-spacing`, how the font glyph is designed (does it naturally have space around it? does it have kerning information?). Then the pseudo elements display type affects if those properties have an effect or not. An SVG, on the other hand, just is the size that it is.

### How to create a spritesheet
This can either be done manually, or by incorporating packages such as [svg-sprite](https://www.npmjs.com/package/svg-sprite) to your build process, or by using CLI tools like [svg-sprite-generator](https://github.com/frexy/svg-sprite-generator). The basic concept still stands though.

#### 1. Create a blank SVG file
```xml
<svg xmlns="http://www.w3.org/2000/svg"></svg>
```

#### 2. Copy-paste the SVG icon
When adding icons to your spritesheet, you should remove the `xmlns` attribute, change the `<svg>` tag to a `<symbol>` tag, and finally add an ID which you will then use to reference the icon (in this case `#brain`).
```xml
<symbol id="brain" viewBox="0 0 512 512">
    <title id="brain-title">Head Side with Brain</title>
    <path d="M160 178.94V176c0 1-.23 1.89-.29 2.85a2.46 2.46 0 0 1 .29.09zM509.21 275c-20.94-47.12-48.44-151.73-73.08-186.75A207.94 207.94 0 0 0 266.09 0H192C86 0 0 86 0 192a191.28 191.28 0 0 0 64 142.82V512h256v-64h64a64 64 0 0 0 64-64v-64h32a32 32 0 0 0 29.21-45zM336 208h-50.94a47.5 47.5 0 0 1 2.94 16 48 48 0 0 1-48 48 47.5 47.5 0 0 1-16-2.94V320h-64v-50.94a47.5 47.5 0 0 1-16 2.94 48 48 0 0 1-48-48c0-1 .23-1.89.29-2.85A47.88 47.88 0 0 1 112 128a48 48 0 0 1 48-48 47.46 47.46 0 0 1 23.53 6.4 47.76 47.76 0 0 1 80.94 0 47.37 47.37 0 0 1 68.59 25.6H336a48 48 0 0 1 0 96z"></path>
</symbol>
```

#### 3. Use the icon on your page
```html
<a title="Illustration of head with brain">
    <svg aria-hidden="true" focusable="false" style="fill: red;">
        <use xlink:href="sprite.svg#brain"></use>
    </svg>
    <span class="access-label">Illustration of head with brain</span>
</a>
```

The `a` element is optional, but here it's used as a wrapper. For styling these icons, you can either use CSS with classes/ID's or inline styles, or you can use SVG attributes. For example, fill colors are controlled with either the `fill: red;` CSS property or the `fill="red"` SVG attribute.

### Important: use meaningful and localized labels
A "magnifying glass" icon can mean "Show the search form" in one context, and "Submit my search query" in a second context. Your text labels should then be "Show the search form" and "Submit search query", not "Search" (too generic) or worse "Magnifying glass" (describes the visuals and not the meaning). On multilingual sites, your text labels should also be localized.

### Caveats

**Internet Explorer doesn't load remote SVGs.** If you use a URL in the `xlink:href` IE will not automatically download the file for you. Check out [SVG For Everybody](https://github.com/jonathantneal/svg4everybody) if this is something you need.

**Same origin policy.** When you use URLs in the `xlink:href` they need to be loaded from the same domain. There is cross-origin protection on SVGs loaded this way. Some people just serve it directly from their static assets. You can also use a proxy.

## Useful links

* [SVGO on Github](https://github.com/svg/svgo)
* [Using Font Awesome sprites](https://fontawesome.com/how-to-use/on-the-web/advanced/svg-sprites)
* [SVG For Everybody polyfill](https://github.com/jonathantneal/svg4everybody)
* [Icon Fonts vs. SVG](https://css-tricks.com/icon-fonts-vs-svg/)
* [svg-sprite @ npm](https://www.npmjs.com/package/svg-sprite)
* [svg-sprite-generator @ npm](https://www.npmjs.com/package/svg-sprite-generator)
