import Vue from 'vue';
import VueResource from 'vue-resource';

require('./scss/index.scss');

Vue.use(VueResource);

const app = new Vue({
    el: '#app',
    data: function () {
        return {
            searchVal: '',
            truckType: '',
            truckSerial: '',
            truckProps: [],
            detailList: [],
            images: [],
            error: false

        };
    },
    methods: {
        search: function(searchVal) {
            app.$http.get('http://localhost:9200/tmhe/_doc/' + searchVal).then((response) => {
                let props = response.data._source.properties.property;
                let mastType = '';
                let liftHeight = '';
                let qpManual = '';
                let omManual = '';
                app.truckProps = [];
                app.error = false;

                //Loop properties to find the ones to use
                for (let i = 0; i < props.length; i++) {
                    if (props[i].key.indexOf('Truck Type') > -1) {
                        app.truckType = props[i].value;
                    };

                    if (props[i].key.indexOf('Truck Serial No') > -1) {
                        app.truckSerial = props[i].value;
                    };

                    if (props[i].key.indexOf('Mast Type') > -1) {
                        mastType = props[i];
                    };

                    if (props[i].key.indexOf('Lift Height') > -1) {
                        liftHeight = props[i];
                    };

                    if (props[i].key.indexOf('QP Manual P/No') > -1) {
                        qpManual = props[i];
                    };

                    if (props[i].key.indexOf('OM Manual P/No') > -1) {
                        omManual = props[i];
                    };
                }
                app.truckProps.push(mastType, liftHeight, qpManual, omManual);
                app.detailList = response.data._source.detailroot.detailgroups;
                app.images = response.data._source.images.image;
            }, response => {
                console.log(response);
                app.error = true;
            });
        },
        showDetails: function() {
            event.currentTarget.classList.toggle('active');
        },
        selectImg: function() {
            let firstImg = document.getElementsByClassName('img-section')[0].firstChild;
            let oldSrc = firstImg.firstChild.src;
            let oldAlt = firstImg.firstChild.alt;

            firstImg.firstChild.src = event.currentTarget.firstChild.src;
            firstImg.firstChild.alt = event.currentTarget.firstChild.alt;

            event.currentTarget.firstChild.src = oldSrc;
            event.currentTarget.firstChild.alt = oldAlt;
        }
    }
});
