### Hamburger menu icon to cross (using pseudo elements and CSS transforms)

**Demo:** https://codepen.io/lethargic/pen/FknJH

**Usage:**

Import `index.css`, and then use the following examples to get everything going.

```html
<ul id="hitbox">
    <li id="cross"></li>
</ul>
```

```javascript
// Using jQuery:

$('#hitbox').click(function(){
    $('#cross').toggleClass('active');
});



// Using vanilla JS:

const el = e => document.getElementById(e);
// This is just a shorthand func for getElementById

el('hitbox').addEventListener('click', () => {
    el('cross').classList.toggle('active');
});
```
