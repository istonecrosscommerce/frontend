$(function() {
    calljson(); 
});
    
function calljson() {
    var json = $('.js-json');
    var loader = json.siblings(".loader");

    if (!json.length) {
        return;
    }

    var url = json.data('url');

    $.ajax({
        type : 'GET',
        dataType : 'json',
        context: this,
        url: url,
    
        beforeSend:function() {             
            loader.addClass("loader__show");
        },
        success:function(data) {
            loader.removeClass("loader__show");
            
            var updatedText = "<span class='title'>" +data[1].title+ "</span><span class='texten'>"+ data[1].body +"</span>";
        
            json.append(updatedText);

            //call js-functions that needs to be hooked again
            
        }, 
        error: function(jqXHR, textStatus, errorThrown) { 
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown); 
        } 
    });
}