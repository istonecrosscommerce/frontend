function checkSSN(ssn) {
    var fyraSista = ssn.toString().substr(-4, 4);
    var dateSSN = ssn.toString().slice(0, -4);

    // Return the SSN and "fyra sista" separately
    return {
        dateSSN: dateSSN,
        fyraSista: fyraSista
    };
}

function todaysDate() {
    var date = Intl.DateTimeFormat('sv-SE').format(new Date());

    // Returns today's date in the format: 20171020
    return {
        y: date.slice(0, 4),
        m: date.slice(5, 7),
        d: date.slice(8, 10)
    };
}

function isLegal(date, ssn) {
    /* Check if person is over 18
     *
     * This is done by simply calculating today's date minus the SSN,
     * and checking the first two digits of the result (which effectively
     * shows the person's age as of today)
     *
     * Example: Person is born is born March 21st 1941
     *          Today's date is October 20th 2017
     *
     *          20171020 - 19410321 = 760 699
     *                                ↑↑ = person is 76 years old - return true
     *
     * Example: Person is born November 3rd 2000
     *          Today's date is January 25th 2018
     *
     *          20180125 - 20001103 = 179 022
     *                                ↑↑ = person is 17 years old - return false
     * */
    var legal = parseInt(date) - parseInt(ssn);
    legal = legal.toString().slice(0, 2);
	legal = parseInt(legal);

    return legal >= 18;
}

$(function() {
    var $SSN = $('input.js-personnummer');

    $SSN.on('keyup', function(e) {
        var keyStrokes = $(this)[0].value.length;

        // Ignore ctrl, alt, shift, command, backspace, and space
        if (
            e.keyCode == 16 ||
            e.keyCode == 17 ||
            e.keyCode == 18 ||
            e.keyCode == 91 ||
            e.keyCode == 8 ||
            e.keyCode == 32
        ) return;

        // If user enters 12 characters, blur the input and start validation
        keyStrokes >= 12 && $SSN.blur();
    });

    $SSN.on('blur', function() {
        var keyStrokes = $(this)[0].value.length;
        var thisSSN = $(this)[0].value;

        if(keyStrokes <= 1) return;

        if(keyStrokes < 10) {
            $SSN.val('');
            return;
        }

        var dateSSN = checkSSN(thisSSN).dateSSN;
        var fyraSista = checkSSN(thisSSN).fyraSista;

        if(dateSSN.substr(0, 2) != '19' && dateSSN.substr(0, 2) != '20') {
            // Check whether to prepend 19 or 20 in front of 10-digit SSN.
            // If the user has entered a 12-digit SSN, this does nothing.
            dateSSN = parseInt(dateSSN.substr(0, 2)) >=
                        parseInt(todaysDate().y.substr(-2, 2))
                ? '19' + dateSSN : '20' + dateSSN;

            $SSN.val(dateSSN + '' + fyraSista);
        }

        var fullDate = (todaysDate().y + todaysDate().m + todaysDate().d);

        var isOver18 = isLegal(fullDate, dateSSN); // isLegal === true if >= 18

        var fullSSN = dateSSN + '' + fyraSista;
        fullSSN = parseInt(fullSSN);

        if(keyStrokes + 1 >= 9 && isOver18) {
            // User has entered at least ten characters, and is 18+ years old
            if(isPersonnummer(fullSSN)) {
                // The user has entered a *valid* SSN
				console.log('You done good!')
            } else {
                console.error('Bad user!')
            }
        }
    });
});
