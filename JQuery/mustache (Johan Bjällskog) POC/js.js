$(function() {
    calljson(); 
});
    
function calljson() {
    var json = $('.js-json');
    var loader = json.siblings(".js-popover-loader");

    if (!json.length) {
        return;
    }

    var url = json.data('url');

    $.ajax({
        type : 'GET',
        dataType : 'json',
        context: this,
        url: url,
    
        beforeSend:function() {
            loader.addClass("loader__show");
        },
        success:function(data) {
            loader.removeClass("loader__show");
            
            //only take the first 10 items from the json, there is 100
            data = data.splice(0,40);

            // Use jQuery to reference our 'persons' script block
            // and grab the HTML contents it contains.
            var template = $('#persons').html();
         
            // Render the template.
            $('.js-json').replaceWith(Mustache.render(template, data));
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown); 
        }
    });
}