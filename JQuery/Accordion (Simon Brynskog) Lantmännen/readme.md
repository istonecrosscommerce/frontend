This is used for an FAQ dropdown where the answer is folded out on question click.

- Provided HTML shows the structure
- Provided css is used for rotating the arrow on the question
- Provided js toggles the question in and out like a dropdown

/Simon Brynskog