;(function($, window, document, undefined) {

    'use strict';

    var pluginName = 'hallonSlider',
        defaults = {
            createLabels: false,
            onChange: undefined,
            prettify: undefined
        };

    function Plugin (element, options) {
        this.element = element;
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    $.extend(Plugin.prototype, {

        init: function() {
            this.settings.value = $(this.element).data('value');
            this.settings.values = $(this.element).data('values');
            this.settings.campaign = $(this.element).data('campaign');
            this.settings.percentage = 0;
            this.settings.selectedIndex = 0;

            this.settings.interactionData = {
                start: {
                    x: 0
                }
            };

            this.settings.scrubberData = {
                x: 0,
                start: {
                    x: 0
                },
                bounds: {
                    left: 0,
                    right: 0
                }
            };

            this._scrubberDownProxy = $.proxy(this.onScrubberDownHandler, this);
            this._scrubberUpProxy = $.proxy(this.onScrubberUpHandler, this);
            this._scrubberMoveProxy = $.proxy(this.onScrubberMoveHandler, this);
            this._backgroundClickProxy = $.proxy(this.onBackgroundClickHandler, this);
            this._onResizeProxy = $.proxy(this.onResizeHandler, this);
            this._onAnimateProxy = $.proxy(this.onAnimateHandler, this);
            this._animation = null;

            $(this.element).find('.hallon-slider__scrubber')
                .on('mousedown touchstart', this._scrubberDownProxy)
                .on('click', function(event) { event.preventDefault(); });

            $(this.element).find('.hallon-slider__background')
                .on('mouseup touchend', this._backgroundClickProxy);

            $(window)
                .on('resize', this._onResizeProxy)
                .trigger('resize');

            if (this.settings.createLabels) {
                this.createLabels();
            } else {
                $(this.element).find('.hallon-slider__labels').remove();
            }

            this.selectValue(this.settings.value);

            // request animation frame polyfill

            var lastTime = 0;
            var vendors = ['ms', 'moz', 'webkit', 'o'];

            for (var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
                window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
                window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
            }

            if (!window.requestAnimationFrame) {
                window.requestAnimationFrame = function(callback, element) {
                    var currTime = new Date().getTime();
                    var timeToCall = Math.max(0, 16 - (currTime - lastTime));
                    var id = window.setTimeout(function() { callback(currTime + timeToCall); }, timeToCall);

                    lastTime = currTime + timeToCall;

                    return id;
                };

                if (!window.cancelAnimationFrame) {

                    window.cancelAnimationFrame = function(id) {

                        clearTimeout(id);
                    };
                }
            }
        },

        createLabels: function() {
            var scope = this;
            var locationDistance = (1 / (this.settings.values.length - 1));
            var $labels = $(this.element).find('.hallon-slider__labels');

            for (var i = 0; i < this.settings.values.length; i++) {

                var value = this.settings.values[i];
                var xPos = (locationDistance * i) * 100;
                var text = value;

                if (this.settings.prettify !== null) {
                    text = this.settings.prettify(text);
                }

                var $label = $('<li class="hallon-slider__label" data-value="' + value + '"></li>');
                $label.text(text);
                $label.css({'left': xPos + '%', 'transform': 'translateX(-50%)'});
                $label.on('click', function(event) { scope.selectValue($(event.target).data('value'));	});
                $label.appendTo($labels);
            }
        },

        onScrubberDownHandler: function(event) {
            event.preventDefault();

            var pointerPosition = this.getPointerPosition(event);
            var interactionData = this.settings.interactionData;
            var scrubberData = this.settings.scrubberData;

            interactionData.start.x = pointerPosition.x;
            scrubberData.start.x = scrubberData.x;

            $(window).on('mousemove touchmove', this._scrubberMoveProxy);
            $(window).on('mouseup touchend', this._scrubberUpProxy);

            this.startAnimation();
        },

        onScrubberMoveHandler: function(event) {
            event.preventDefault();

            var $el = $(this.element);
            var $scrubber = $el.find('.hallon-slider__scrubber');

            var pointerPosition = this.getPointerPosition(event);
            var interactionData = this.settings.interactionData;
            var scrubberData = this.settings.scrubberData;

            scrubberData.x = (scrubberData.start.x + (pointerPosition.x - interactionData.start.x));

            if (scrubberData.x < scrubberData.bounds.left) {
                scrubberData.x = scrubberData.bounds.left;
            }

            if (scrubberData.x > scrubberData.bounds.right) {
                scrubberData.x = scrubberData.bounds.right;
            }

            this.settings.percentage = (scrubberData.x / ($el.width() - $scrubber.width()));
        },

        onScrubberUpHandler: function(event) {
            event.preventDefault();

            this.selectValueByPercent();

            $(window).off('mousemove touchmove', this._scrubberMoveProxy);
            $(window).off('mouseup touchend', this._scrubberUpProxy);

            this.stopAnimation();
        },

        onResizeHandler: function(event) {
            var $el = $(this.element);
            var $scrubber = $el.find('.hallon-slider__scrubber');

            this.settings.scrubberData.bounds.right = ($el.width() - $scrubber.width());
            this.settings.scrubberData.x = (this.settings.scrubberData.bounds.right * this.settings.percentage);

            this.updateView(false);
        },

        onBackgroundClickHandler: function(event) {
            if (event.target == $(this.element).find('.hallon-slider__background').get(0)) {

                var pointerPosition = this.getPointerPosition(event);
                var parentOffset = $(event.target).parent().offset();
                var $el = $(this.element);

                pointerPosition.x -= parentOffset.left;
                pointerPosition.y -= parentOffset.top;

                this.settings.percentage = (pointerPosition.x / $el.width());

                this.selectValueByPercent();
            }
        },

        startAnimation: function() {
            this.onAnimateHandler();
        },

        stopAnimation: function() {
            window.cancelAnimationFrame(this._animation);
            this._animation = null;
        },

        onAnimateHandler: function() {
            this.updateView(false);
            this._animation = window.requestAnimationFrame(this._onAnimateProxy);
        },

        selectValue: function(value) {
            var selectedIndex = -1;

            for (var i = 0; i < this.settings.values.length; i++) {
                if (this.settings.values[i] == value) {
                    selectedIndex = i;
                    break;
                }
            }

            if (selectedIndex == -1) {
                selectedIndex = 0;
            }

            var locationDistance = (1 / (this.settings.values.length - 1));

            this.settings.selectedIndex = selectedIndex;
            this.settings.value = value;
            this.settings.percentage = (locationDistance * selectedIndex);
            this.settings.scrubberData.x = (this.settings.scrubberData.bounds.right * this.settings.percentage);

            this.updateView(true);
            this.callChangeHandler();
        },

        selectValueByPercent: function() {
            var locationDistance = (1 / (this.settings.values.length - 1));
            var distances = [];

            for (var i = 0; i < this.settings.values.length; i++) {

                var locationPercentage = (locationDistance * i);
                var distance = Math.abs(this.settings.percentage - locationPercentage);

                distances.push({index: i, distance: distance, percentageLocation: locationPercentage});
            }

            distances.sort(function(a, b) {

                return (a.distance > b.distance) ? 1 : -1;
            });

            this.selectValue(this.settings.values[distances[0].index]);
        },

        updateView: function(animate) {
            var scrubberData = this.settings.scrubberData;
            var $scrubber = $(this.element).find('.hallon-slider__scrubber');
            var props = {
                'left': scrubberData.x
            };

            if (animate == true) {
                $scrubber.stop().clearQueue().animate(props, 100);
            } else {
                $scrubber.css(props);
            }
        },

        callChangeHandler: function() {
            if (this.settings.onChange !== undefined) {

                this.settings.onChange.apply(true, [
                    this.settings.value,
                    this.isValueCampaign(this.settings.value),
                    this.selectedIndex
                ]);
            }
        },

        isValueCampaign: function(value) {
            var isCampaign = false;

            if (this.settings.campaign !== undefined) {

                for (var i = 0; i<this.settings.campaign.length; i++) {

                    if (this.settings.value == this.settings.campaign[i]) {

                        isCampaign = true;
                        break;
                    }
                }
            }

            return isCampaign;
        },

        getPointerPosition: function(event) {
            var position = {

                x: 0,
                y: 0
            };

            if (event.type == 'touchstart' || event.type == 'touchmove' || event.type == 'touchend' || event.type == 'touchcancel') {

                var touch = event.originalEvent.touches[0] || event.originalEvent.changedTouches[0];

                position.x = touch.pageX;
                position.y = touch.pageY;

            } else if (event.type == 'mousedown' || event.type == 'mouseup' || event.type == 'mousemove' || event.type == 'mouseover'|| event.type=='mouseout' || event.type=='mouseenter' || event.type=='mouseleave') {

                position.x = event.pageX;
                position.y = event.pageY;
            }

            return position;
        }
    });

    $.fn[pluginName] = function(options) {
        return this.each(function() {
            if (!$.data(this, 'plugin_' + pluginName)) {
                $.data(this, 'plugin_' + pluginName, new Plugin(this, options));
            }
        });
    };

})(jQuery, window, document);
